package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.entity.Course;
import com.hibernate.entity.Instructor;
import com.hibernate.entity.InstructorDetail;
import com.hibernate.entity.Student;

public class CreateCoursesDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// create the objects
			Instructor tempInstructor = new Instructor("Susan","Public","susan.public@gmail.com");
			
			
			InstructorDetail tempInstructorDetail = new InstructorDetail(
					"http://www.youtube.com",
					"Video Games"
					);
			
			tempInstructor.setInstructorDetail(tempInstructorDetail);
			
			
			// start a transaction
			session.beginTransaction();
			
			// get the instructor from db
			int theId = 1;
			Instructor instructor = session.get(Instructor.class ,theId);
			
			// create some courses
			Course firstCourse = new Course("Air Guitar - The ultimate guide");
			Course secondCourse = new Course("The Pinball Masterclass");
			
			// add courses to instructor
			instructor.add(firstCourse);
			instructor.add(secondCourse);
			
			// save the courses
			session.save(firstCourse);
			session.save(secondCourse);
			
			// commit the transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			
		} finally {
			
			// add clean up code
			session.close();
			
			factory.close();
		}
	}

}
