package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.hibernate.entity.Course;
import com.hibernate.entity.Instructor;
import com.hibernate.entity.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// start a transaction
			session.beginTransaction();
			
			int theId = 1;
			
			// option 2: Hibernate query HQL
			
			Query<Instructor> query = session.createQuery
					("select i from Instructor i JOIN FETCH i.courses "
							+"where i.id=:instructor",
							Instructor.class);
			
			// set parameter on query
			query.setParameter("instructor", theId);
			
			// execute query and get instructor 
			Instructor instructor = query.getSingleResult();	
			
			System.out.println("GentHoxha debugmode: Instructor: "+instructor);
			
			
			
			// commit the transaction
			session.getTransaction().commit();
			
			// close the session 
			session.close();
			
			
			// instructor.getCourses() is on lazy loading and doesn't work when session is closed ,
			// but as i loaded first when session was opened its saved on memory
			System.out.println("GentHoxha debugmode: Courses: "+instructor.getCourses());
			
			System.out.println("GentHoxha debugmode: Done!");
			
		} finally {
			
			// add clean up code
			session.close();
			
			factory.close();
		}
	}

}
