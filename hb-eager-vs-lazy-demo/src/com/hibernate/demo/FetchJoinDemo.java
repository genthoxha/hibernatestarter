package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.entity.Course;
import com.hibernate.entity.Instructor;
import com.hibernate.entity.InstructorDetail;
import com.hibernate.entity.Student;

public class FetchJoinDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// start a transaction
			session.beginTransaction();
			
			// get the instructor from db
			int theId = 1;
			Instructor instructor = session.get(Instructor.class ,theId);
			
			System.out.println("GentHoxha debugmode: Instructor: "+instructor);
			
			// get course for the instructor
			System.out.println("GentHoxha debugmode: Courses: "+instructor.getCourses());
			
			
			// commit the transaction
			session.getTransaction().commit();
			
			// close the session 
			session.close();
			
			
			// instructor.getCourses() is on lazy loading and doesn't work when session is closed ,
			// but as i loaded first when session was opened its saved on memory
			System.out.println("GentHoxha debugmode: Courses: "+instructor.getCourses());
			
			System.out.println("GentHoxha debugmode: Done!");
			
		} finally {
			
			// add clean up code
			session.close();
			
			factory.close();
		}
	}

}
