package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.entity.Course;
import com.hibernate.entity.Instructor;
import com.hibernate.entity.InstructorDetail;
import com.hibernate.entity.Review;
import com.hibernate.entity.Student;

public class AddCoursesForMaryDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		// create session
		Session session = factory.getCurrentSession();
		
		try {			
			
			// start a transaction
			session.beginTransaction();
			int maryID = 1;
			
			System.out.println("--------------------------------------------------------------------");
			// get the student mary from database
			Student maryStudent = session.get(Student.class,maryID);
			System.out.println("Marys current courses: "+maryStudent.getCourses());
			
			
			// create more courses
			System.out.println("-------------------Added courses to mary-------------------------------------------------");
			Course springCourse = new Course("Mary's spring course");
			Course java2EECourse = new Course("Mary's java 2EE course");
			Course mongoDBCourse = new Course("Mary's MongoDB course");
			
			// add student to courses
			springCourse.addStudent(maryStudent);
			java2EECourse.addStudent(maryStudent);
			mongoDBCourse.addStudent(maryStudent);
			
/*	No need to do this
 * 			maryStudent.addCourses(springCourse);
			maryStudent.addCourses(java2EECourse);
			maryStudent.addCourses(mongoDBCourse);*/
			
			// save the courses
			session.save(springCourse);
			session.save(java2EECourse);
			session.save(mongoDBCourse);
			
			System.out.println("--------------------All students in Spring------------------------------------------------");
			System.out.println("-----Spring---------"+springCourse.getStudents());
			
			System.out.println("--------------------All students in java2EECourse------------------------------------------------");
			System.out.println("-----java2EECourse-----------------------------------"+java2EECourse.getStudents());
			
			System.out.println("--------------------MARY COURSES------------------------------------------------");
			System.out.println("-----java2EECourse-----------------------------------"+maryStudent.getCourses().size());
			
			
			
			
			
			
			
			
			
			System.out.println("--------------------------------------------------------------------");
			
					
					
					
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done!");
		}
		finally {
			
			// add clean up code
			session.close();
			
			factory.close();
		}
	}

}





