package com.hibernate.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {

		
		String jdbcUrl = "jdbc:mysql://localhost:3306/hb-01-one-to-one-uni?useSSL=false";
		String user = "hbstudent";
		String pass = "hbstudent";
		
		try {
			System.out.println("Connecting to database: "+jdbcUrl);
			
			Connection myConn = DriverManager.getConnection(jdbcUrl,user,pass);
			
			System.out.println("Connection successful!!! \nThis is just a confirmation that we have"
					+ " the right url , username , password and also more importantly \njdbc driver that we have in our lib directory"
					+ " is associated with our classpath and we can get a valid connection "
					+ "");
			
		}catch(Exception ex) {
			
		}
		
	}

}
