package com.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// start transaction
			session.beginTransaction();

			// query students
			List<Student> theStudents = session.createQuery("from Student").getResultList();

			// displayStudents
			displayStudents(theStudents);

			// query students: lastName = 'Doe';

			theStudents = session.createQuery("from Student s where s.lastName='Doe'").getResultList();

			// displayStudents
			System.out.println("\nThe students who have last name of Doe");
			displayStudents(theStudents);

			// suqery students: lastName='Doe' OR firstName='Daffy'
			theStudents = session.createQuery("from Student s where" + " s.lastName='Doe' OR s.firstName='Daffy'")
					.getResultList();

			System.out.println("\nThe students who have : lastName='Doe' OR firstName='Daffy");
			// displayStudents
			displayStudents(theStudents);
			
			// query students where email LIKE '%luv2code.com'
			theStudents = session.createQuery("from Student s where s.email LIKE '%luv2code.com'").getResultList();
			
			System.out.println("query students where email LIKE %luv2code.com");
			// displayStudents
			displayStudents(theStudents);

			// commit transaction
			session.getTransaction().commit();

		} finally {
			factory.close();
		}
	}

	private static void displayStudents(List<Student> theStudents) {
		// display the students
		for (Student tempStudent : theStudents) {
			System.out.println(tempStudent);
		}
	}

}
