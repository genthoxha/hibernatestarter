package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.entity.Instructor;
import com.hibernate.entity.InstructorDetail;
import com.hibernate.entity.Student;

public class CreateDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// create the objects
			Instructor tempInstructor = new Instructor("Gent","Hoxha","hgent92@gmail.com");
			
			Instructor secondInstructor = new Instructor("Madhu","Patel","madhu@luv2code.com");
			
			InstructorDetail tempInstructorDetail = new InstructorDetail(
					"http://www.genthoxha.com",
					"Luv 2 code!!"
					);
			
			InstructorDetail secondInstructorDetails = new InstructorDetail(
					"madhu@luv2code.com",
					"Dashuria per kod"
					);
			
			
			// associate the objects together
			tempInstructor.setInstructorDetail(tempInstructorDetail);
			
			secondInstructor.setInstructorDetail(secondInstructorDetails);
			
			// begin a transaction
			session.beginTransaction();
			
			// save the instructor
			// 
			// Note: this will ALSO save the details object
			// because of CascadeType.ALL
			System.out.println("Saving instructor "+tempInstructor);
			session.save(tempInstructor);
			
			session.save(secondInstructor);
			
			// commit the transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			
		} finally {
			factory.close();
		}
	}

}
